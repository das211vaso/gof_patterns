﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoF_Patterns
{
    class RobotId
    {
        public int Id { get; set; }

        public override string ToString()
        {
            return "Robot id = " + Id; 
        }

        public RobotId(int id)
        {
            Id = id;
        }
    }
}
