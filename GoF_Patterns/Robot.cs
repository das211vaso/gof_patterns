﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoF_Patterns
{
    class Robot: IPrototype
    {
        public RobotId RobotId { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }

        public Robot()
        {
            RobotId = new RobotId(new Random().Next());
            Model = "NS-5";
            Color = "White";
        }

        public Robot(RobotId robotId, string model, string color)
        {
            RobotId = robotId;
            Model = model;
            Color = color;
        }

        public override string ToString()
        {
            return this.RobotId.ToString() + "; Model = " + this.Model + "; Color = " + this.Color;
        }

        public Robot ShallowCopy()
        {
            return (Robot)this.MemberwiseClone();
        }

        public Robot DeepCopy()
        {
            var copiedRobot = (Robot)this.MemberwiseClone();
            copiedRobot.RobotId = new RobotId(this.RobotId.Id);

            return copiedRobot;
        }
    }
}
