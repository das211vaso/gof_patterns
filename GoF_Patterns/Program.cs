﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoF_Patterns
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Prototype pattern");
            Console.WriteLine();

            Robot NS4_1 = new Robot();

            Robot NS4_2 = NS4_1.ShallowCopy();
            Robot NS4_3 = NS4_1.DeepCopy();

            Console.WriteLine("Before changes to NS-4");
            Console.WriteLine(NS4_1);
            Console.WriteLine(NS4_2);
            Console.WriteLine(NS4_3);

            NS4_1.RobotId.Id = 1;
            NS4_1.Model = "NS4.0";
            NS4_1.Color = "Red";

            NS4_2 = NS4_1.DeepCopy();

            Console.WriteLine();
            Console.WriteLine("After changes to NS-4");
            Console.WriteLine(NS4_1);
            Console.WriteLine(NS4_2);
            Console.WriteLine(NS4_3);

            Console.ReadLine();
        }
    }
}
